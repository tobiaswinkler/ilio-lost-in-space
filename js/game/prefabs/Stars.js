var Stars = function(game, x, y, key, frame) {
    key = 'star';
    Phaser.Sprite.call(this, game, x, y, key, frame);
    this.anchor.setTo(0.5);
    this.flip = false;

    this.animations.add('flyStar', [0]);

    this.game.physics.arcade.enableBody(this);
    this.body.allowGravity = false;

    this.checkWorldBounds = true;
    this.onOutOfBoundsKill = true;

    this.events.onRevived.add(this.onRevived, this);

};

Stars.prototype = Object.create(Phaser.Sprite.prototype);
Stars.prototype.constructor = Stars;

Stars.prototype.onRevived = function() {

    if(this.flip == true){
        this.scale.setTo(-1,1);

    }else{
        this.scale.setTo(1,1);
    }
    this.body.velocity.y = 50;
    this.animations.play('flyStar', 10, true);
};



