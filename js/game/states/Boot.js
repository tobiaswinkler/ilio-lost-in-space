
var IlioLostInSpace = function() {};

IlioLostInSpace.Boot = function() {};

IlioLostInSpace.Boot.prototype = {

  loadImages: function () {
    this.load.image('logo', 'assets/images/logo.png');
    this.load.image('preloadbar', 'assets/images/loadbar.png');


  },
  preload: function() {
   this.loadImages();
  },
  create: function() {

    this.input.maxPointers = 1;

    if (this.game.device.desktop) {

      this.scale.pageAlignHorizontally = true;
      this.scale.pageAlignVeritcally = true;
      this.scale.refresh();
    } else {
      this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
      this.scale.minWidth = 568;
      this.scale.minHeight = 600;
      this.scale.maxWidth = 2048;
      this.scale.maxHeight = 1536;
      this.scale.forceLandscape = true;
      this.scale.pageAlignHorizontally = true;
      this.scale.setScreenSize(true);
    }

    this.state.start('Preloader');
  }
};